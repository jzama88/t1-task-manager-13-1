package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.model.Project;

import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project add(Project project);

    Project create (String name);

    Project create (String name, String description);

    void clear();

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusById(String id, Status status);

}
