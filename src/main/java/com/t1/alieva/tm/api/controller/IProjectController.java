package com.t1.alieva.tm.api.controller;

import com.t1.alieva.tm.model.Project;

public interface IProjectController {

    void createProject();

    void clearProject();

    void showProjects();

    void showProject(Project project);

    void showProjectById();

    void showProjectByIndex();

    void updateProjectByIndex();

    void updateProjectById();

    void removeProjectById();

    void removeProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

}
