package com.t1.alieva.tm.controller;

import com.t1.alieva.tm.api.controller.IProjectTaskController;
import com.t1.alieva.tm.api.service.IProjectTaskService;
import com.t1.alieva.tm.service.ProjectTaskService;
import com.t1.alieva.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject(){
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(projectId,taskId);
        System.out.println("[OK]");
    }

    @Override
    public void unbindTaskToProject(){
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("[ENTER PROJECT ID:]");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK ID:]");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.unbindTaskFromProject(projectId,taskId);
        System.out.println("[OK]");
    }

}
