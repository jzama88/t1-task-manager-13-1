package com.t1.alieva.tm;


import com.t1.alieva.tm.component.Bootstrap;

public final class Application {

     public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
